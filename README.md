# debian

````
 Buzz(1.1; 1996-03-14),
 Rex(1.2; 1996-10-28),
 Bo(1.3; 1997-05-01),
 Hamm(2.0; 1998-07-24),
 Slink(2.1; 1999-03-09),
 Potato(2.2; 2000-08-15),
 Woody(3.0; 2002-07-19),
 Sarge(3.1; 2005-06-06),
 Etch(4.0; 2007-04-08),
 Lenny(5.0; 2009-02-14),
 Squeeze(6.0; 2011-02-06),
 Wheezy(7; 2013-05-04),
--
 Jessie(8; 2015-04-25),
 Stretch (9; 2017-06-17),
 Buster (10; 2019-07-06).
 Bullseye (11;).
 Bookworm (12;) (next release).
````

Introduction. systemd is a system and service manager for Linux. It is the default init system for Debian since Debian 8 ("jessie").

net-tools 1.60 stretch, i.e. 9.


## aarch64

https://jensd.be/1126/linux/cross-compiling-for-arm-or-aarch64-on-debian-or-ubuntu



# Debian (64 bits, debootstrap from manjaro) 

````
base64/bin/dash: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=be6397ddffc3e384c505d7deab3bed190c884df9, for GNU/Linux 3.2.0, stripped

md5sum base64/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 
27c2fb9dc3678baa5b80552a3e508f2b  base64/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
````

# manjaro x86_64

````
/bin/bash: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=9483da49f2e17070c1df9a75d509e09211e96769, for GNU/Linux 4.4.0, not stripped

/lib/ld-linux-x86-64.so.2: ELF 64-bit LSB shared object, x86-64, version 1 (GNU/Linux), static-pie linked, BuildID[sha1]=47da73e90a3ede988e228212d8247a94f9da36a0, stripped
````


# Devuan amd64, ascii

````
bin/dash: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=2e947be051b84a5a8799dfb8af24abe900e1ab56, stripped

d9dfbbdee7ef7def6689ecb14fea722b  lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
````


